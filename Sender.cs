﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sosnovskiy.WindowsForms.SendingData
{
    public partial class Sender : Form
    {
        // private Action<string> _actionS;

        public Sender()
        {
            InitializeComponent();
        }

        private void btn_Send_Click(object sender, EventArgs e)
        {
            // 1
            // Receiver receiver = new Receiver(tbSource.Text);
            // receiver.ShowDialog();

            // 2
            /*
            Data.TextData = tbSource.Text;
            Receiver receiver = new Receiver();

            if (receiver.ShowDialog() == DialogResult.OK)
            {
                tbSource.Text = Data.TextData;
            }
            */

            // 3
            Receiver receiver = new Receiver();
            Data.MyAction = TextWrite;
            Data.MyAction += receiver.WriteDestination;
            Data.MyAction(tbSource.Text);

            receiver.ShowDialog();
        }

        private void TextWrite(string text)
        {
            tbSource.Text = text;
        }
    }
}
