﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sosnovskiy.WindowsForms.SendingData
{
    public partial class Receiver : Form
    {
        // private string _text;
        // private Action<string> _action;

        public Receiver()
        {
            InitializeComponent();
        }

        /*
        public Receiver(string text) : this()
        {
            _text = text;
            tbDestination.Text = text;
        }
        */

        //private void Receiver_Load(object sender, EventArgs e)
        //{
        //    tbDestination.Text = Data.TextData;
        //}

        private void btn_OK_Click(object sender, EventArgs e)
        {
            // Data.TextData = tbDestination.Text;

            Data.MyAction(tbDestination.Text);

            Close();
        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        public void WriteDestination(string text)
        {
            tbDestination.Text = text;
        }
    }
}
